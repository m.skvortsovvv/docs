# Список источников

*Note:* Список будет пополняться.

### Практика
- Autocode. Java: SQL and JDBC - [https://autocode.lab.epam.com/student/group/78](https://autocode.lab.epam.com/student/group/78)
- Autocode. Java: Servlets - [https://autocode.lab.epam.com/student/group/79](https://autocode.lab.epam.com/student/group/79)

### Методическое пособие
[Основы разработки ИС](https://vk.com/doc-154193295_518816775)

### Рекомендуемые материалы

#### Java
- К. Хорстманн - Java. Библиотека профессионала. 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java. Очень большой, подробный и мой любимый. Содержит главу по JDBC.
- И. Блинов, В. Романчик - Java. Методы программирования.\
Есть главы по сервлетам и JDBC.\
Можно свободно скачать тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books). 

#### Spring
- Ю. Козмина, Р. Харроп - Spring 5 для профессионалов\
Классический толстенный том о Spring. Мне, опять же, очень нравится, но может показаться слишком большим.
- DI Motivation\
Репозиторий с примером эволюции от Hello World до проекта с внедрением зависимостей через Spring.
Именно на его основе я вам объяснял, зачем нужно внедрение зависимостей, только мы заменили тему на выборы в США.
[https://github.com/thejerome/SpringTutorialSpringCore](https://github.com/thejerome/SpringTutorialSpringCore)
- Коллекция гайдов от разработчиков Spring: [https://spring.io/guides](https://spring.io/guides)\
Очень много гайдов, некоторые из которых вам даже подойдут. Некоторые переведены на русский вот здесь: [https://spring-projects.ru/guides/](https://spring-projects.ru/guides/)
- На Baeldung многие статьи и гайды относятся к Spring: [https://www.baeldung.com/spring-tutorial](https://www.baeldung.com/spring-tutorial)

#### BD. SQL
- С. Куликов - Реляционные базы данных в примерах.\
Книга сравнительно небольшая, однако охватывает все основные аспекты реляционных БД.\
Можно свободно скачать тут: http://svyatoslav.biz/relational_databases_book_download/
- SQL Bolt.[https://sqlbolt.com/](https://sqlbolt.com/)\
Тренажер по SQL - используйте для того, чтобы разобраться в основных командах SQL
- Технострим от разработчика Аллодов из mail.ru по БД (долго, подробно): [https://www.youtube.com/watch?v=SfYaAQ9-RnE&list=PLrCZzMib1e9oOFQbuOgjKYbRUoA8zGKnj&index=1](https://www.youtube.com/watch?v=SfYaAQ9-RnE&list=PLrCZzMib1e9oOFQbuOgjKYbRUoA8zGKnj&index=1)\
Вот тут вам подробно расскажут о деталях, которые мы опустили - что такое изоляция транзакций, как устроены индексы, что такое explain и т.д.

#### UML
- М. Фаулер - UML. Основы. Краткое руководство по стандартному языку объектного моделирования\
Кратко, но основательно изложено практически все, что нужно от UML разработчику ПО в профессиональной деятельности.



